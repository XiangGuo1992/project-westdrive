## Contributors:
Main Contributors and maintainers of the project:
* Maximilian Alexander Wächter : Co-developer of Project Westdrive
* Prof. Dr. rer. nat. Peter König : Projects' main supervisor
* Prof. Dr. rer. nat. Gordon Pipa : Projects' secondary supervisor

---
Project assistants:
*  Max Pingle : Developement
*  Fabian : Developement
*  Philip : 3D Model Design