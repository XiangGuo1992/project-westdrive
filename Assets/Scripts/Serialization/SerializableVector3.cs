﻿
/// <summary>
/// Since unity doesn't flag the Vector3 as serializable, we
/// need to create our own version. This one will automatically convert
/// between Vector3 and SerializableVector3
/// </summary>


[ProtoBuf.ProtoContract]
public class PBVector3
{
    [ProtoBuf.ProtoIgnore]
    public UnityEngine.Vector3 vector3 = new UnityEngine.Vector3();
    public PBVector3() { }
    public PBVector3(UnityEngine.Vector3 source)
    {
        vector3 = source;
    }
    [ProtoBuf.ProtoMember(1, Name = "x")]
    public float X
    {
        get { return vector3.x; }
        set { vector3.x = value; }
    }
    [ProtoBuf.ProtoMember(2, Name = "y")]
    public float Y
    {
        get { return vector3.y; }
        set { vector3.y = value; }
    }
    [ProtoBuf.ProtoMember(3, Name = "z")]
    public float Z
    {
        get { return vector3.z; }
        set { vector3.z = value; }
    }
    public static implicit operator UnityEngine.Vector3(PBVector3 i)
    {
        return i.vector3;
    }
    public static implicit operator PBVector3(UnityEngine.Vector3 i)
    {
        return new PBVector3(i);
    }
}

//namespace unityengine
//{
//    [zeroformattable]
//    public struct vector3
//    {
//        [index(0)]
//        public float x;
//        [index(1)]
//        public float y;
//        [index(2)]
//        public float z;

//        public vector3(float x, float y, float z)
//        {
//            this.x = x;
//            this.y = y;
//            this.z = z;
//        }
//    }
//}
//[System.Serializable]
//public struct SerializableVector3
//{
//    /// <summary>
//    /// x component
//    /// </summary>
//    public float x;

//    /// <summary>
//    /// y component
//    /// </summary>
//    public float y;

//    /// <summary>
//    /// z component
//    /// </summary>
//    public float z;

//    /// <summary>
//    /// Constructor
//    /// </summary>
//    /// <param name="rX"></param>
//    /// <param name="rY"></param>
//    /// <param name="rZ"></param>
//    public SerializableVector3(float rX, float rY, float rZ)
//    {
//        x = rX;
//        y = rY;
//        z = rZ;
//    }

//    /// <summary>
//    /// Returns a string representation of the object
//    /// </summary>
//    /// <returns></returns>
//    public override string ToString()
//    {
//        return String.Format("[{0}, {1}, {2}]", x, y, z);
//    }

//    /// <summary>
//    /// Automatic conversion from SerializableVector3 to Vector3
//    /// </summary>
//    /// <param name="rValue"></param>
//    /// <returns></returns>
//    public static implicit operator Vector3(SerializableVector3 rValue)
//    {
//        return new Vector3(rValue.x, rValue.y, rValue.z);
//    }

//    /// <summary>
//    /// Automatic conversion from Vector3 to SerializableVector3
//    /// </summary>
//    /// <param name="rValue"></param>
//    /// <returns></returns>
//    public static implicit operator SerializableVector3(Vector3 rValue)
//    {
//        return new SerializableVector3(rValue.x, rValue.y, rValue.z);
//    }
//}